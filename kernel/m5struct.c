/*
 * Copyright (c) 2005
 * The Regents of The University of Michigan
 * All Rights Reserved
 *
 * This code is part of the M5 simulator, developed by Nathan Binkert,
 * Erik Hallnor, Steve Raasch, and Steve Reinhardt, with contributions
 * from Ron Dreslinski, Dave Greene, Lisa Hsu, Kevin Lim, Ali Saidi,
 * and Andrew Schultz.
 *
 * Permission is granted to use, copy, create derivative works and
 * redistribute this software and such derivative works for any
 * purpose, so long as the copyright notice above, this grant of
 * permission, and the disclaimer below appear in all copies made; and
 * so long as the name of The University of Michigan is not used in
 * any advertising or publicity pertaining to the use or distribution
 * of this software without specific, written prior authorization.
 *
 * THIS SOFTWARE IS PROVIDED AS IS, WITHOUT REPRESENTATION FROM THE
 * UNIVERSITY OF MICHIGAN AS TO ITS FITNESS FOR ANY PURPOSE, AND
 * WITHOUT WARRANTY BY THE UNIVERSITY OF MICHIGAN OF ANY KIND, EITHER
 * EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE. THE REGENTS OF THE UNIVERSITY OF MICHIGAN SHALL NOT BE
 * LIABLE FOR ANY DAMAGES, INCLUDING DIRECT, SPECIAL, INDIRECT,
 * INCIDENTAL, OR CONSEQUENTIAL DAMAGES, WITH RESPECT TO ANY CLAIM
 * ARISING OUT OF OR IN CONNECTION WITH THE USE OF THE SOFTWARE, EVEN
 * IF IT HAS BEEN OR IS HEREAFTER ADVISED OF THE POSSIBILITY OF SUCH
 * DAMAGES.
 */

#include <stddef.h>
#include <linux/sched.h>
#include <linux/mm_types.h>
#include <linux/mm.h>
#include <linux/fs.h>
#include <linux/path.h>
#include <linux/dcache.h>

/*
 * Structure size information
 */
const int thread_info_size = sizeof(struct thread_info);
const int thread_info_task = offsetof(struct thread_info, task);

const int task_struct_size = sizeof(struct task_struct);
const int task_struct_pid = offsetof(struct task_struct, pid);
const int task_struct_tgid = offsetof(struct task_struct, tgid);
const int task_struct_mm = offsetof(struct task_struct, active_mm);
const int mm_struct_size = sizeof(struct mm_struct);
const int mm_struct_start_code = offsetof(struct mm_struct, start_code);
const int mm_struct_end_code = offsetof(struct mm_struct, end_code);
const int mm_struct_start_brk = offsetof(struct mm_struct, start_brk);
const int mm_struct_brk = offsetof(struct mm_struct, brk);
const int mm_struct_start_stack = offsetof(struct mm_struct, start_stack);
const int mm_struct_mmap_base = offsetof(struct mm_struct, mmap_base);
const int mm_struct_start_data = offsetof(struct mm_struct, start_data);
const int mm_struct_end_data = offsetof(struct mm_struct, end_data);
const int mm_struct_stack_vm = offsetof(struct mm_struct, stack_vm);
const int mm_struct_mmap = offsetof(struct mm_struct, mmap);
const int task_struct_start_time = offsetof(struct task_struct, start_time);
const int task_struct_comm = offsetof(struct task_struct, comm);
const int task_struct_comm_size = TASK_COMM_LEN;

const int vm_area_struct_size = sizeof(struct vm_area_struct);
const int vm_area_struct_vm_start = offsetof(struct vm_area_struct, vm_start);
const int vm_area_struct_vm_end = offsetof(struct vm_area_struct, vm_end);
const int vm_area_struct_vm_next = offsetof(struct vm_area_struct, vm_next);
const int vm_area_struct_vm_file = offsetof(struct vm_area_struct, vm_file);

const int file_struct_size = sizeof(struct file);
const int file_struct_f_path = offsetof(struct file, f_path);

const int path_size = sizeof(struct path);
const int path_dentry = offsetof(struct path, dentry);

const int dentry_size = sizeof(struct dentry);
const int dentry_d_iname = offsetof(struct dentry, d_iname);

const int vm_growsup = VM_GROWSUP;
const int vm_growsdown = VM_GROWSDOWN;
